# personal-website

The source code for ian-s-mcb's website. The site is built using the
Hugo static site generator and is live at https://iansmcb.gq.

### Updating theme

From the root of your site:
```bash
git submodule foreach git pull origin main
```
